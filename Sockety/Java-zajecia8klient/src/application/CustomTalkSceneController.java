package application;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class CustomTalkSceneController {
	private Client client;
	
	@FXML
	Button sendButton;
	@FXML
	Button connectButton;
	@FXML
	Button disconnectButton;
	@FXML
	TextArea readingTextArea;
	@FXML
	TextField writingTextField;
	@FXML
	TextField loginTextField;
	
	@FXML
	public void initialize(){
		readingTextArea.setEditable(false);
		writingTextField.setDisable(true);
		sendButton.setDisable(true);
		disconnectButton.setDisable(true);
		client = new Client(loginTextField, writingTextField,readingTextArea);
		client.start();
	}
	
	
	@FXML
	private void connectButtonOnAction(){
		client.login();
		connectButton.setDisable(true);
		writingTextField.setDisable(false);
		sendButton.setDisable(false);
		disconnectButton.setDisable(false);
	}
	@FXML
	private void disconnectButtonOnAction(){
		client.disconnect();
		try {
			client.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		writingTextField.setDisable(true);
		sendButton.setDisable(true);
		disconnectButton.setDisable(true);
	}
	@FXML
	private void sendButtonOnAction(){
		client.send();
	}
}
