package application;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Client extends Thread {
	private Socket clientSocket = null;
	private PrintStream os = null;
	private DataInputStream is = null;
	private ClientReader read = null;
	private final int PORT = 6000;
	private final String HOST = "localhost";
	private boolean isWorking;
	private TextArea readingTextArea = null;
	private TextField loginTextField;
	private TextField writingTextField;
	
	public Client(TextField loginTextField, TextField writingTextField, TextArea readingTextArea){
			this.loginTextField = loginTextField;
			this.writingTextField = writingTextField;
			this.readingTextArea = readingTextArea;
			isWorking = true;
	}
	
	@Override
	public void run(){
		try {
			clientSocket = new Socket(HOST, PORT);
			os = new PrintStream(clientSocket.getOutputStream());
		    is = new DataInputStream(clientSocket.getInputStream()); 
		    System.out.println("jestem");
		} catch (UnknownHostException e) {
			System.err.println("Couldn't connect to host -Error in class Client ");
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection -Error in class Client");
		}
		
		if(clientSocket != null && os != null && is != null){
			read = new ClientReader(is, readingTextArea);
			read.start();
			while(isWorking){
			}
			try {
				read.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void send(){
		try{
		os.println(MessageType.MESSAGE + " " + writingTextField.getText());
		//writingTextField.clear();
		}catch(NullPointerException e){
			System.out.println("null");
		}
	}
	
	public void login(){
		try{
			os.println(MessageType.LOGIN + " " + loginTextField.getText());
		}catch(NullPointerException e){
			System.out.println("kolejny null2");
		}
	}
	
	public void disconnect(){
		try{
		os.println(MessageType.LOGOUT);
		isWorking = false;
			read.setIsWorking(isWorking);
		}catch(NullPointerException e){
			System.out.println("kolejny null");
		}
	}
}
