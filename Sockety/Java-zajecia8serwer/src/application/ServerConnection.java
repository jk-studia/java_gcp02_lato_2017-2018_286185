package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TextArea;

public class ServerConnection extends Thread {

	  private ServerSocket server = null;
	  private Socket clientSocket = null;
	  private final int maxClientsCount = 10;
	  private final Client[] threads = new Client[maxClientsCount];
	  private final int PORT = 6000;
	  private TextArea textArea;
	  
	 public ServerConnection( TextArea textArea) {
		this.textArea = textArea;
	}
	  @Override
	  public void run() {
		Logger logger = new CommunicatorLogger(textArea);
	    try {
	      server = new ServerSocket(PORT);
	    } catch (IOException e) {
	      System.out.println(e);
	    }catch(NullPointerException e){
	    	System.out.println("nie zalozono serwera");
	    }
	    System.out.println("dzialam");
	    while (true) {
	      try {
	        clientSocket = server.accept();
	        int i = 0;
	        for (i = 0; i < maxClientsCount; i++) {
	          if (threads[i] == null) {
		        
	            threads[i] = new Client(clientSocket, threads);
	       //     threads[i].addClientAddedListener(Client->logger.log(MessageType.LOGIN, Client));
	        //    threads[i].addClientSendMessageListener(Client->logger.log(MessageType.MESSAGE, Client));
	         //   threads[i].addClientRemovedListener(Client->logger.log(MessageType.LOGOUT, Client));
	            threads[i].start();
	            break;
	          }
	        }
	        if (i == maxClientsCount) {
	          PrintStream os = new PrintStream(clientSocket.getOutputStream());
	          os.println("Server jest pelny");
	          os.close();
	          clientSocket.close();
	        }
	      } catch (IOException e) {
	        System.out.println(e);
	      }
	    }
	  }
	}
