package application;

import javafx.scene.control.TextArea;

public class CommunicatorLogger implements Logger {
	
	TextArea textArea = null;
	
	public CommunicatorLogger(TextArea textArea){
		this.textArea = textArea;
	}
	
	@Override
	public void log(int messageType, Client client) {
		if(messageType == MessageType.LOGIN){
			textArea.appendText(client+" nadal wiadomosc");
		}
		if(messageType == MessageType.LOGOUT){
			textArea.appendText(client+" sie wylogowal");
		}
		if(messageType == MessageType.MESSAGE){
			textArea.appendText(client+" sie zalogowal");
		}
	}
}
