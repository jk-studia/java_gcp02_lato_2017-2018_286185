package application;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class ServerController {
	
	
	@FXML
	public void initialize() {
		stopButton.setDisable(true);
		textArea.setEditable(false);
	}
	@FXML
	private Button startButton;
	@FXML
	private Button stopButton;
	@FXML
	TextArea textArea;
	private ServerConnection server = new ServerConnection(textArea);

	
	@FXML
	public void startOnAction(){
		server.start();
		startButton.setDisable(true);
		stopButton.setDisable(false);
	}
	@FXML
	public void stopOnAction(){
		
		stopButton.setDisable(true);
	}
	
}
