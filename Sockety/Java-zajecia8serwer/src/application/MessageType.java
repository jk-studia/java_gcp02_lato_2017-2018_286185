package application;

public class MessageType {
	public static final int LOGIN = 1;
	public static final int LOGOUT = 2;
	public static final int MESSAGE = 3;
}
