 package application;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Client extends Thread {

	  private String clientName = null;
	  private DataInputStream is = null;
	  private PrintStream os = null;
	  private Socket clientSocket = null;
	  private final Client[] threads;
	  private int maxClientsCount;
	  private BufferedReader reader = null;
	  
		private List<ClientListener> clientAddedListeners = new ArrayList<>();
		public void addClientAddedListener(ClientListener listener){
			clientAddedListeners.add(listener);
		}
		public void removeClientAddedListener(ClientListener listener){
			clientAddedListeners.remove(listener);
		}
		
		private List<ClientListener> clientSendMessageListeners = new ArrayList<>();
		public void addClientSendMessageListener(ClientListener listener){
			clientSendMessageListeners.add(listener);
		}
		public void removeClientSendMessageListener(ClientListener listener){
			clientSendMessageListeners.remove(listener);
		}
		
		private List<ClientListener> clientRemovedListeners = new ArrayList<>();
		public void addClientRemovedListener(ClientListener listener){
			clientRemovedListeners.add(listener);
		}
		public void removeClientRemovedListener(ClientListener listener){
			clientRemovedListeners.remove(listener);
		}
	  
	  public Client(Socket clientSocket, Client[] threads) {
	    this.clientSocket = clientSocket;
	    this.threads = threads;
	    maxClientsCount = threads.length;
	  }
	  @Override
	  public void run() {
	    int maxClientsCount = this.maxClientsCount;
	    Client[] threads = this.threads;
	    
	    try {
	      is = new DataInputStream(clientSocket.getInputStream());
	      os = new PrintStream(clientSocket.getOutputStream());
	      reader = new BufferedReader(new InputStreamReader(is));
	      String name = null;
	      while (true) {
	        name = reader.readLine();
	        if (name.indexOf('@') == -1) {
	            break;
	          } else {
	            os.println("The name should not contain '@' character.");
	          }
	      }

	      os.println("Welcome " + name);
	      synchronized (this) {
	        for (int i = 0; i < maxClientsCount; i++) {
	          if (threads[i] != null && threads[i] == this) {
	            clientName = "@"+ name;
	        //    for(ClientListener cl: clientAddedListeners)
            //		cl.handle(this);
	            break;
	          }
	        }
	        for (int i = 0; i < maxClientsCount; i++) {
	          if (threads[i] != null && threads[i] != this) {
	            threads[i].os.println(name + " entered the chat room");
	          }
	        }
	      }
	      
	      
	      while (true) {
	        String line = reader.readLine();
	        if (line.startsWith("2")) {
	          break;
	        }

	          synchronized (this) {
	            for (int i = 0; i < maxClientsCount; i++) {
	              if (threads[i] != null && threads[i].clientName != null) {
	                threads[i].os.println(name + ": " + line);
	              }
	            }
	          }
	      }
	      synchronized (this) {
	        for (int i = 0; i < maxClientsCount; i++) {
	          if (threads[i] != null && threads[i] != this && threads[i].clientName != null) {
	            threads[i].os.println(name+ " is leaving the chat room");
	          }
	        }
	      }

	      synchronized (this) {
	        for (int i = 0; i < maxClientsCount; i++) {
	          if (threads[i] == this) {
	            threads[i] = null;
	          }
	        }
	      }
	      is.close();
	      os.close();
	      clientSocket.close();
	    } catch (IOException e) {
	    }
	  }
	  public String toString(){
		  return clientName;
	  }
	}