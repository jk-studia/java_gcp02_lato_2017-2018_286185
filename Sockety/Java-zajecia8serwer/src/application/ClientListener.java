package application;

@FunctionalInterface
public interface ClientListener {
	void handle(Client client);
}
