package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	private Stage window;
	
	
	@Override
	public void start(Stage primaryStage) {
		window = primaryStage;
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("Server.fxml"));
			Parent root = loader.load();
			Scene scene = new Scene(root);
			window.setScene(scene);
			window.setTitle("Communicator Server");
			window.show();

		}catch(Exception e){
			System.out.println("Loading server scene -ERROR in class Main");
			System.exit(0);
		}

	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
