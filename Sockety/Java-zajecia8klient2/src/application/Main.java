package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	private Stage window;
	private Scene scene;
	
	@Override
	public void start(Stage primaryStage) {
		window = primaryStage;
		try{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("CustomTalkScene.fxml"));
		Parent root = loader.load();
		scene = new Scene(root);
		window.setScene(scene);
		window.setTitle("Communicator");
		window.show();
		window.setResizable(false);
		}catch(Exception e){
			System.out.println("Loading scene -ERROR");
			System.exit(0);
		}

	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
