package application;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javafx.scene.control.TextArea;

public class ClientReader extends Thread {
	private BufferedReader reader;
	private boolean isWorking;
	private TextArea readingTextArea;
	public ClientReader(DataInputStream is, TextArea readingTextArea, boolean isWorking){
		this.readingTextArea = readingTextArea;
		reader = new BufferedReader(new InputStreamReader(is));
		this.isWorking = isWorking;
	}
	
	public void setIsWorking(boolean isWorking){

		this.isWorking = isWorking;
	}
	
	@Override
	public void run(){
		while(isWorking){
			try {
				if(reader.readLine()!=null){
					readingTextArea.appendText(reader.readLine()+"\n");
					if(reader.readLine() == "DISCONECT"){
						isWorking = false;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
