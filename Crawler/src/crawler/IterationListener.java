package crawler;
@FunctionalInterface

public interface IterationListener {
	void handle(int i);
}
