package crawler;
@FunctionalInterface

public interface StudentListener {
	void handle(Student student);
}
