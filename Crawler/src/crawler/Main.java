package crawler;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		Crawler crawler = new Crawler();
		crawler.addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
		crawler.addIterationCompletedListener(iteration->System.out.println("Zakonczono iteracje "));
		crawler.addStudentAddedListener((Student)->Student.toString());
		crawler.addStudentRemovedListener((Student)->Student.toString());
		crawler.run();
	}
}
