package application;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class CustomBarChart extends AnchorPane {
	private int[] count = new int[6];
	private CategoryAxis xAxis = new CategoryAxis();
	private NumberAxis yAxis = new NumberAxis();
	private BarChart<String, Number> barChart;
	static XYChart.Series<String, Number> series;
	
	public CustomBarChart(Stage primaryStage){
		xAxis.setCategories(FXCollections.<String>observableArrayList("2.0", "3.0", "3.5", "4.0", "4.5", "5.0"));
		xAxis.setLabel("Mark"); 
		yAxis.setLabel("Count");
		barChart = new BarChart<>(xAxis, yAxis);  
		barChart.setTitle("Distribution of Marks");
        
		for(int i = 0; i < 6; i++){
        	count[i] = 0;
        }
        
		series = new XYChart.Series<String,Number>();
		series.setName("Marks");
	    series.getData().add(new XYChart.Data<>("2.0",count[0]));
	    series.getData().add(new XYChart.Data<>("3.0",count[1]));
	    series.getData().add(new XYChart.Data<>("3.5",count[2]));
	    series.getData().add(new XYChart.Data<>("4.0",count[3]));
	    series.getData().add(new XYChart.Data<>("4.5",count[4]));
	    series.getData().add(new XYChart.Data<>("5.0",count[5]));
		
		barChart.getData().add(series);
		this.getChildren().add(barChart);
		CustomBarChart.setTopAnchor(barChart, 10.0);
		CustomBarChart.setLeftAnchor(barChart, 10.0);
		CustomBarChart.setRightAnchor(barChart, 10.0);
		CustomBarChart.setBottomAnchor(barChart, 10.0);
	}
	
	public void update(ObservableList<Student> student){
		for(int i = 0; i < 6; i++){
        	count[i] = 0;
        }
		double mark;
		for(Student s: student){
			mark = s.getMark();
			if(mark == 2.0) count[0]++;
			if(mark == 3.0) count[1]++;
			if(mark == 3.5) count[2]++;
			if(mark == 4.0) count[3]++;
			if(mark == 4.5) count[4]++;
			if(mark == 5.0) count[5]++;
		}
		int j = 0;
		for(XYChart.Data<String, Number> newData: series.getData()){
			newData.setYValue(count[j]);
			j++;
		}
	}
}
