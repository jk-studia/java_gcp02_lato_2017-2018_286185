package application;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class CustomTableView extends AnchorPane {
	private TableView<Student> tableView;
	private TableColumn<Student, String> firstNameColumn;
	private TableColumn<Student, String> lastNameColumn;
	private TableColumn<Student, Double> markColumn;
	private TableColumn<Student, Integer> ageColumn;
	
	public CustomTableView(Stage primaryStage, ObservableList<Student> observableList){
		
		markColumn = new TableColumn<>("Mark");
		markColumn.setMinWidth(100);
		markColumn.setCellValueFactory(new PropertyValueFactory<>("mark"));
		
		firstNameColumn = new TableColumn<>("First Name");
		firstNameColumn.setMinWidth(100);
		firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
		
		lastNameColumn = new TableColumn<>("Last Name");
		lastNameColumn.setMinWidth(100);
		lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
		
		ageColumn = new TableColumn<>("Age");
		ageColumn.setMinWidth(100);
		ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
		
		tableView = new TableView<>();
		tableView.setItems(observableList);
		tableView.getColumns().add(markColumn);
		tableView.getColumns().add(firstNameColumn);
		tableView.getColumns().add(lastNameColumn);
		tableView.getColumns().add(ageColumn);
		
		tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		this.getChildren().addAll(tableView);
		
		CustomTableView.setTopAnchor(tableView, 10.0);
		CustomTableView.setLeftAnchor(tableView, 10.0);
		CustomTableView.setRightAnchor(tableView, 10.0);
		CustomTableView.setBottomAnchor(tableView, 10.0);
	}
	
	public TableView<Student> getTableView(){
		return tableView;
	}
	
}
