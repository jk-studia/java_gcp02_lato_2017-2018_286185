package application;
	
import java.io.File;
import java.io.IOException;
import java.util.List;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception{
			primaryStage.setTitle("CrawlerGUI");

			ObservableList<LogItem> logList = FXCollections.observableArrayList();
			ObservableList<Student> observableStudentList = FXCollections.observableArrayList();
			List<Student> studentList = null;
			Logger logger = new GUILogger(logList);
			
			String file1 = "students1.txt";
			File file = new File(file1);
			
			try{
				studentList  = StudentsParser.parse(file);
			}catch(IOException e){
				System.out.println("Error File");
			}
			for(Student s: studentList){
				observableStudentList.add(s);
				logger.log("ADDED", s);
			}
			

			// creating scene
			VBox layout = new VBox();
			CustomMenuBar customMenuBar = new CustomMenuBar(primaryStage);
			CustomTabPane customTabPane = new CustomTabPane(primaryStage, logList, logger, observableStudentList);
			layout.getChildren().addAll(customMenuBar, customTabPane);
			
			Scene scene = new Scene(layout, 500, 500);
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
