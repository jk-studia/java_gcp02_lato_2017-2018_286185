package application;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class CustomMenuBar extends MenuBar {
	private Menu programMenu, aboutMenu;
	private MenuItem closeItem;
	Alert alert;
	
	public CustomMenuBar(Stage primaryStage){
		
		programMenu = new Menu("Program");
		closeItem = new MenuItem("Close");
		closeItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN));
		closeItem.setOnAction(e->primaryStage.close());
		programMenu.getItems().add(closeItem);
		
		aboutMenu = new Menu("About");
		aboutMenu.getItems().add(new MenuItem());
		aboutMenu.setOnShown(e->showInfo(primaryStage));
		
        this.prefWidthProperty().bind(primaryStage.widthProperty());
        this.prefWidthProperty().bind(primaryStage.heightProperty());
        this.getMenus().addAll(programMenu, aboutMenu);
	}
	
	
	public void showInfo(Stage primaryStage){
        alert = new Alert(Alert.AlertType.INFORMATION, "Author: Jaros�aw Ko�mic" , ButtonType.OK);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.setTitle("About");
        alert.setHeaderText("Program Details");
        alert.setContentText("Author: Jaros�aw Ko�mic");
        alert.initOwner(primaryStage);
        alert.show();
	}
}
