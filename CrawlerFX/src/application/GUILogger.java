package application;

import java.util.Calendar;
import java.util.GregorianCalendar;
import javafx.collections.ObservableList;

import java.text.SimpleDateFormat;

public class GUILogger implements Logger {
	private LogItem logItem;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
	private Calendar calendar = new GregorianCalendar();
	private ObservableList<LogItem> logList;
	public GUILogger(ObservableList<LogItem> newLogList){
		logList = newLogList;
	}
	@Override
	public void log(String status, Student student){
		logItem = new LogItem();
		if(status == "ADDED"){
			logItem.setMessage(sdf.format(calendar.getTime())+" [NEW] "+student);
			logList.add(logItem);
		}
		if(status == "REMOVED"){
			logItem.setMessage(sdf.format(calendar.getTime())+" [REMOVED] "+student);
			logList.add(logItem);
		}
	}
}
