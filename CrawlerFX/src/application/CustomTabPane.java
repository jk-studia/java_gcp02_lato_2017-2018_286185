package application;

import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CustomTabPane extends AnchorPane {
	
	private TabPane tabPane;
	private Tab studentsTab, logTab, histogramTab;
	private CustomTableView customTableView;
	private CustomLogTab customLogTab;
	private VBox vBox;
	private HBox hBoxButton;
	private Button addButton, removeButton;
	private ObservableList<Student> studentList = null;
	private CustomBarChart customBarChart;
	
	public CustomTabPane(Stage primaryStage, ObservableList<LogItem> logList, Logger logger, ObservableList<Student> newStudentList){
		tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		studentList = newStudentList;
		studentsTab = new Tab("Students");
		customTableView = new CustomTableView(primaryStage, studentList);
		studentsTab.setContent(customTableView);
		
		// Creating Buttons
		addButton = new Button("Add");
		addButton.setOnAction(e->addStudents(studentList, logger));
		removeButton = new Button("Remove");
		removeButton.setOnAction(e->removeStudents(studentList, logger));
		
		hBoxButton = new HBox();
		hBoxButton.getChildren().addAll(addButton, removeButton);
		
		logTab = new Tab("Log");
		customLogTab = new CustomLogTab(primaryStage, logList);
		logTab.setContent(customLogTab);
		
		histogramTab = new Tab("Histogram");
		customBarChart = new CustomBarChart(primaryStage);
		customBarChart.update(studentList);
		histogramTab.setContent(customBarChart);
		
		tabPane.getTabs().addAll(studentsTab, logTab, histogramTab);
		
		vBox = new VBox();
		vBox.getChildren().addAll(tabPane, hBoxButton);
		

		this.getChildren().addAll(vBox);
		CustomTabPane.setTopAnchor(vBox, 10.0);
		CustomTabPane.setLeftAnchor(vBox, 10.0);
		CustomTabPane.setRightAnchor(vBox, 10.0);
		CustomTabPane.setBottomAnchor(vBox, 10.0);
	}
	
	public void addStudents(ObservableList<Student> studentList, Logger logger){
		Student student = AddButtonBox.addBox();
		logger.log("ADDED", student);
		studentList.add(student);
		customBarChart.update(studentList);
	}
	
	public void removeStudents(ObservableList<Student> studentList, Logger logger){
		ObservableList<Student> selectedStudent;
		selectedStudent = customTableView.getTableView().getSelectionModel().getSelectedItems();
		for(Student student: selectedStudent){
			logger.log("REMOVED", student);
			studentList.remove(student);
		}
		customBarChart.update(studentList);
	}
}