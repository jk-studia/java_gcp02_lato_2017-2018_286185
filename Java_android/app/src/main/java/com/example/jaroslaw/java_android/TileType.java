package com.example.jaroslaw.java_android;

public enum TileType {
    EMPTY,
    WALL,
    HEAD,
    TAIL,
    FOOD;
}
