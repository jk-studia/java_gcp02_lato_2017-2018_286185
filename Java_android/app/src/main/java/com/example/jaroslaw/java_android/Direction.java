package com.example.jaroslaw.java_android;

public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
}
