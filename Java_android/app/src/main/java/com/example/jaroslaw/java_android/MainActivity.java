package com.example.jaroslaw.java_android;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener{

    private Engine gameEngine;
    private SnakeView snakeView;
    private final Handler handler = new Handler();

    private float prevX, prevY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gameEngine = new Engine();
        gameEngine.initializeGame();
        snakeView = (SnakeView) findViewById(R.id.snakeView);
        snakeView.setViewMap(gameEngine.getMap());
        snakeView.setOnTouchListener(this);
        snakeView.invalidate();
        startUpdateHandler();
    }

    // poruszanie sie weza
    public void startUpdateHandler(){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                gameEngine.update();
                if (gameEngine.getCurrentGameState() == GameState.RUNNING) {
                    handler.postDelayed(this, 300);
                }
                if(gameEngine.getCurrentGameState() == GameState.END){
                    gameLost();
                }

                snakeView.setViewMap(gameEngine.getMap());
                snakeView.invalidate();
            }
        }, 300);
    }

    // wyswietlenie komunikatu o przegranej
    public void gameLost(){
        Toast.makeText(this, "Game Over", Toast.LENGTH_SHORT).show();
    }

    // poruszanie wężem
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                prevX = event.getX();
                prevY = event.getY();

                break;
            case MotionEvent.ACTION_UP:
                float newX = event.getX();
                float newY = event.getY();

                if(Math.abs(newX - prevX)>Math.abs(newY - prevY)){
                    if(newX>prevX){
                        gameEngine.updateDirection(Direction.RIGHT);
                    }else{
                        gameEngine.updateDirection(Direction.LEFT);
                    }
                }
                else{
                    if(newY>prevX){
                        gameEngine.updateDirection(Direction.DOWN);
                    }
                    else{
                        gameEngine.updateDirection(Direction.UP);
                    }
                }
                break;
        }
        return true;
    }
}
