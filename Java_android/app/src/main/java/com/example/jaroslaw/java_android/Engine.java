package com.example.jaroslaw.java_android;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Engine {
    private static final int GameWidth = 28;
    private static final int GameHeight = 42;

    private List<Coordinate> walls = new ArrayList<>();
    private List<Coordinate> snake = new ArrayList<>();
    private List<Coordinate> food = new ArrayList<>();


    private Direction currentDirection = Direction.DOWN;
    private GameState currentGameState = GameState.RUNNING;

    private Random rand = new Random();


    public GameState getCurrentGameState() {
        return currentGameState;
    }


    public TileType[][] getMap(){
        TileType[][] map = new TileType[GameWidth][GameHeight];

        for(int x = 0; x < GameWidth; x++){
            for(int y = 0; y < GameHeight;y++){
                map[x][y] = TileType.EMPTY;
            }
        }

        for(Coordinate wall: walls){
            map[wall.getX()][wall.getY()] = TileType.WALL;
        }

        for(Coordinate s: snake) {
            if (!s.equals(snake.get(0)))
                map[s.getX()][s.getY()] = TileType.TAIL;
            else
                map[s.getX()][s.getY()] = TileType.HEAD;
        }

        for(Coordinate f: food){
            map[f.getX()][f.getY()] = TileType.FOOD;

        }
        return map;
    }

    public void initializeGame(){
        // tworzenie nowego węża
        snake.clear();
        snake.add(new Coordinate(7,7));

        // tworzenie ścian
        for(int x = 0; x < GameWidth; x++){
            walls.add(new Coordinate(x, 0));
            walls.add(new Coordinate(x, GameHeight-1));
        }

        for(int y = 1; y < GameHeight; y++){
            walls.add(new Coordinate(0,y));
            walls.add(new Coordinate(GameWidth-1, y));
        }

        // tworzenie jedzenia
        addFood();
    }

    // dodanie jedzenia dla węża
    public void addFood(){
        Coordinate foodCoor = null;
        boolean added = false;
        while(!added){
            int x = 1+rand.nextInt(GameWidth-2);
            int y = 1+rand.nextInt(GameHeight-2);
            boolean collision = false;
            foodCoor = new Coordinate(x,y);

            for(Coordinate s: snake){
                if(s.equals(foodCoor))
                    collision = true;
                    break;
            }
            added = !collision;
        }
        food.add(foodCoor);
    }

    public void update(){
        switch(currentDirection){
            case UP:
                updateSnake(0,-1);
                break;
            case RIGHT:
                updateSnake(1,0);
                break;
            case DOWN:
                updateSnake(0,1);
                break;
            case LEFT:
                updateSnake(-1,0);
                break;
        }

        for(Coordinate wall: walls){
            if(snake.get(0).equals(wall)){
                currentGameState = GameState.END;
            }
        }

        Coordinate eated = null;
        for(Coordinate f: food){
            if(snake.get(0).equals(f)){
                eated = f;
            }
        }

        if(eated != null){
            food.remove(eated);
            addFood();
            snake.add(new Coordinate(snake.get(snake.size()-1).getX(),snake.get(snake.size()-1).getY()));
        }
    }

    public void updateSnake(int x, int y){
        for (int i = snake.size()-1; i > 0; i--){
            snake.get(i).setX(snake.get(i-1).getX());
            snake.get(i).setY(snake.get(i-1).getY());
        }
        snake.get(0).setX(snake.get(0).getX()+x);
        snake.get(0).setY(snake.get(0).getY()+y);
    }

    public void updateDirection(Direction newDirection){
        if(Math.abs(newDirection.ordinal()-currentDirection.ordinal())%2==1){
            currentDirection = newDirection;
        }
    }
}
