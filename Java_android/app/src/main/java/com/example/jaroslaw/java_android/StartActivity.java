package com.example.jaroslaw.java_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class StartActivity extends AppCompatActivity {
    // Starting activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    // go to next activity after clicking on button - launch games
    public void startOnClick(View view){
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }
}
