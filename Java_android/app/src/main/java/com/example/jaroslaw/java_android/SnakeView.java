package com.example.jaroslaw.java_android;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class SnakeView  extends android.view.View{
    private Paint paint = new Paint();
    private TileType viewMap[][];


    public void setViewMap(TileType[][] viewMap) {
        this.viewMap = viewMap;
    }

    public SnakeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float tileSizeX = canvas.getWidth() / viewMap.length;
        float tileSizeY = canvas.getHeight() / viewMap[0].length;

        float circleSize =  Math.min(tileSizeX,tileSizeY)/2;

        if(viewMap != null){
            for(int x = 0; x < viewMap.length; x++){
                for(int y = 0; y < viewMap[x].length; y++){
                    switch(viewMap[x][y]){
                        case EMPTY:
                            paint.setColor(Color.WHITE);
                            break;
                        case WALL:
                            paint.setColor(Color.BLUE);
                            break;
                        case HEAD:
                            paint.setColor(Color.RED);
                            break;
                        case TAIL:
                            paint.setColor(Color.GREEN);
                            break;
                        case FOOD:
                            paint.setColor(Color.YELLOW);
                            break;

                    }
                    canvas.drawCircle(x*tileSizeX+tileSizeX/2f+circleSize/2, y*tileSizeY+tileSizeY/2f+circleSize/2,circleSize,paint);
              }
            }
        }
    }
}
