package threads;

import crawler.Crawler;

public class MyThread extends Thread {
	private static int ID = 0;
	private int iID;
	private Crawler crawler = null;
	private String link = null;
	public MyThread(Crawler newCrawler, String newLink){
		ID++;
		iID = ID;
		crawler = newCrawler;
		link = newLink;
	}
	
	public Crawler getCrawler(){
		return crawler;
	}
	
	@Override
	public void run(){
		crawler.run(iID, link);
	}
}
