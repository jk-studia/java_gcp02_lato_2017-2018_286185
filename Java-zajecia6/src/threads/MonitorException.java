package threads;

public class MonitorException extends Exception{
	private String statement;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MonitorException(String newStatement){
		statement = newStatement;
	}
	public void showErrorAndClose(){
		System.out.println(statement);
		System.exit(0);
	}
}
