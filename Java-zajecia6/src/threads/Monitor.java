package threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import crawler.Crawler;
import crawler.IterationListener;
import crawler.StudentListener;

public class Monitor {
	
	private List<String> linkList = null;
	private Crawler crawler = null;
	private int numberOfThreads;
	private Scanner userNumberOfThreads = new Scanner(System.in);
	private MyThread[] threads = null;
	private boolean isWorking;

	private List<StudentListener> studentAddedListeners = new ArrayList<>();
	public void addStudentAddedListener(StudentListener listener){
		studentAddedListeners.add(listener);
	}
	public void removeStudentAddedListener(StudentListener listener){
		studentAddedListeners.remove(listener);
	}
	
	private List<StudentListener> studentRemovedListeners = new ArrayList<>();
	public void addStudentRemovedListener(StudentListener listener){
		studentRemovedListeners.add(listener);
	}
	public void removeStudentRemovedListener(StudentListener listener){
		studentRemovedListeners.remove(listener);
	}
	
	
	public Monitor(List<String> newLinkList){
		linkList = newLinkList;
		isWorking = true;
	}
	
	public boolean getIsWorking(){
		return isWorking;
	}
	
	public void start() throws InterruptedException{
		for(int i = 0; i < numberOfThreads; i++)
			threads[i].start();
		for(int i = 0; i < numberOfThreads; i++){
			Thread.sleep(3000);
			threads[i].getCrawler().postCancel();
		}
			
		
	}
	
	public void run() throws MonitorException, InterruptedException{
		crawler = new Crawler();
		crawler.addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
		crawler.addIterationCompletedListener(iteration->System.out.println("Zakonczono iteracje "));
		crawler.addStudentAddedListener((Student)->{for(StudentListener el: studentAddedListeners){ el.handle(Student);}});
		crawler.addStudentRemovedListener((Student)->{for(StudentListener el: studentRemovedListeners){ el.handle(Student);}});
		
		System.out.println("Podaj liczbe watkow");
		numberOfThreads = userNumberOfThreads.nextInt();
		userNumberOfThreads.close();
		if(numberOfThreads < linkList.size())
			throw new MonitorException("Za mala liczba watkow");
		if(numberOfThreads > linkList.size())
			numberOfThreads = linkList.size();

		threads = new MyThread[numberOfThreads];
		for(int i = 0; i < numberOfThreads; i++){
			threads[i] = new MyThread(crawler, linkList.get(i));
		}
		start();
	
		cancel();
	}
	
	public void cancel() throws InterruptedException{
		System.out.println("Zakonczenie pracy");
		isWorking = false;
		for(int i = 0; i < numberOfThreads; i++){
			threads[i].getCrawler().postCancel();
			threads[i].join();
		}
	}
}
