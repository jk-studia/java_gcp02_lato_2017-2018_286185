package threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import crawler.Logger;
import crawler.Student;

public class ParallelLogger implements Logger {
	List<Logger> loggerList = new ArrayList<>();
	LinkedBlockingQueue<String> statusQueue = new LinkedBlockingQueue<>();
	LinkedBlockingQueue<Student> studentQueue = new LinkedBlockingQueue<>();
	String giveStatus;
	Student giveStudent = null;
	
	private boolean isWorking;
	public ParallelLogger(Logger log, boolean isMonitorWorking) {
		loggerList.add(log);
		isWorking = isMonitorWorking;
		Thread thread = new Thread(new Runnable(){
			@Override
			public void run() {

				while(isWorking){
					while(!statusQueue.isEmpty() && !studentQueue.isEmpty()){
						giveStatus = statusQueue.poll();
						giveStudent = studentQueue.poll();
						loggerList.get(0).log(giveStatus, giveStudent);
					}
				}
			}
		});
		thread.start();
	}
	
	@Override
	public void log(String status, Student student) {
		if(status == "ADDED"){
			try {
				statusQueue.put(status);
				studentQueue.put(student);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if(status == "REMOVED"){
			try {
				statusQueue.put(status);
				studentQueue.put(student);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
