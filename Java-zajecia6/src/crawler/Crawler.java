package crawler;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Crawler {
	private List<IterationListener> iterationStartedListeners = new ArrayList<>();
	public void addIterationStartedListener(IterationListener listener){
		iterationStartedListeners.add(listener);
	}
	public void removeIterationStartedListener(IterationListener listener){
		iterationStartedListeners.remove(listener);
	}
	
	private List<IterationListener> iterationCompletedListeners = new ArrayList<>();
	public void addIterationCompletedListener(IterationListener listener){
		iterationCompletedListeners.add(listener);
	}
	public void removeIterationCompletedListener(IterationListener listener){
		iterationCompletedListeners.remove(listener);
	}
	
	private List<StudentListener> studentAddedListeners = new ArrayList<>();
	public void addStudentAddedListener(StudentListener listener){
		studentAddedListeners.add(listener);
	}
	public void removeStudentAddedListener(StudentListener listener){
		studentAddedListeners.remove(listener);
	}
	
	private List<StudentListener> studentRemovedListeners = new ArrayList<>();
	public void addStudentRemovedListener(StudentListener listener){
		studentRemovedListeners.add(listener);
	}
	public void removeStudentRemovedListener(StudentListener listener){
		studentRemovedListeners.remove(listener);
	}
	
	private List<Student> studentsList = null;
	private Set<Student> studentsSet = new HashSet<Student>();
	
	void showSorted(List<Student> students){
		for(Student student: students){
			System.out.println(student.getMark()+" "+student.getFirstName()+" "+student.getLastName()+" "+student.getAge());
		}
	}
	
	
	double extractMark(ExtremumMode mode)
	{
		double max = 0.0;
		double min = 6.0;
		
		if(mode == ExtremumMode.MAX){
			for(Student student : studentsSet){
				if(student.getMark()>max)
					max = student.getMark();
			}
			return max;
		}
		else 
			if(mode == ExtremumMode.MIN){
				for(Student student : studentsSet){
					if(student.getMark()<min)
						min = student.getMark();
				}
			return min;
			}
		return 0;
	}
	
	int extractAge(ExtremumMode mode){
		int max = 0;
		int min = 100;
		
		if(mode == ExtremumMode.MAX){
			for(Student student : studentsSet){
				if(student.getAge()>max)
					max = student.getAge();
			}
			
			return max;
		}
		if(mode == ExtremumMode.MIN){
			for(Student student : studentsSet){
				if(student.getMark()<min)
					min = student.getAge();
			}		
			return min;
		}
		return 0;
	}
	
	List<Student> extractStudents(OrderMode mode){
		List<Student> tempList= new ArrayList<Student>();
	
		for(Student student : studentsSet)
			tempList.add(student);
		
		System.out.print("ORDERED by ");
		
		if(mode == OrderMode.LAST_NAME){		
			System.out.println("LAST NAME");	
			tempList.sort(new LastNameComparatorAscending());
			//tempList.sort(new LastNameComparatorDescending());
			return tempList;
		}
		if(mode == OrderMode.AGE){	
			System.out.println("AGE");
			tempList.sort(new AgeComparatorAscending());
			//tempList.sort(new AgeComparatorDescending());
			return tempList;
		}
		if(mode == OrderMode.MARK){	
			System.out.println("MARK");
			//tempList.sort(new MarkComparatorAscending());
			tempList.sort(new MarkComparatorDescending());
			return tempList;
		}
		return null;
	}
	
	private File file = null;
	
	private boolean threadAlive = true;
	
	public void postCancel(){
		threadAlive = false;
	}
	
	public synchronized void run(int ID, String link){
		int iteration = 1;
		System.out.println("Teraz dziala watek: " + ID + "\n");
		threadAlive = true;
		System.out.println(link);
		file = new File(link);
		while(threadAlive == true){
			try{
				studentsList  = StudentsParser.parse(file);
			}
			catch(IOException e){
				System.out.println("Error File");
			}
			
			for(IterationListener el :iterationStartedListeners){
				el.handle(iteration);
			}
			
			for(StudentListener el: studentAddedListeners){
				
				for(Student student : studentsList){	
					if(studentsSet==null){
						studentsSet.add(student);
						el.handle(student);
					}
					
					else if(!studentsSet.contains(student)){
						studentsSet.add(student);
						el.handle(student);
					}
						
				}
				
			}
			for(StudentListener el : studentRemovedListeners){
				List<Student> studentsToRemove = new ArrayList<Student>();
				
				for(Student student : studentsSet)
				{
					if(studentsList == null)
					{
						studentsToRemove.add(student);
						el.handle(student);
					}
					else if(!studentsList.contains(student))
					{
						studentsToRemove.add(student);
						el.handle(student);
					}
				}
				
				for(Student student : studentsToRemove)
					studentsSet.remove(student);
			}
			System.out.println("\nSTUDENT SET:");
			for(Student student: studentsSet)
				System.out.println(student);
			
			System.out.println("Age: " + "<" + extractAge(ExtremumMode.MIN) + "," + extractAge(ExtremumMode.MAX) + ">");
			System.out.println("Mark: " + "<" + extractMark(ExtremumMode.MIN) + "," + extractMark(ExtremumMode.MAX) + ">");
	//		showSorted(extractStudents(OrderMode.MARK));
	//		showSorted(extractStudents(OrderMode.LAST_NAME));
	//		showSorted(extractStudents(OrderMode.AGE));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			for(IterationListener el :iterationCompletedListeners){
				el.handle(iteration);
			}
			System.out.println("\n");
			iteration++;
		}
	}
}
