package crawler;

import java.util.ArrayList;
import java.util.List;
import threads.Monitor;
import threads.MonitorException;
import threads.ParallelLogger;

public class Main {

	public static void main(String[] args) {
		List<String> linkList = new ArrayList<>();
		for(int i = 1; i <= 10; i++)
			linkList.add("students"+i+".txt");
		Monitor monitor = new Monitor(linkList);
		final Logger logger = new ParallelLogger(new ConsoleLogger(), monitor.getIsWorking());

		
		try {
			monitor.addStudentAddedListener((Student)->logger.log("ADDED", Student));
			monitor.addStudentRemovedListener((Student)->logger.log("REMOVED", Student));
			monitor.run();
		} catch (MonitorException e) {
			e.printStackTrace();
			e.showErrorAndClose();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
}
