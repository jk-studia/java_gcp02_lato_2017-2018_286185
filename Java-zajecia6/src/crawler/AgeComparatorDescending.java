package crawler;


import java.util.Comparator;

public class AgeComparatorDescending implements Comparator<Student>{

	@Override
	public int compare(Student student1, Student student2) {
		return Integer.compare(student2.getAge(), student1.getAge());
	}

}
