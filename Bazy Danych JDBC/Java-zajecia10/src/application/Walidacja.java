package application;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Walidacja {
	public final static String YEAR2000="20[0,1]{1}[0-9]{1}";
	public final static String YEAR19xx="19[0-9]{2}";
	public final static String TYPE="[a-zA-z]{45}";
	public final static String ID="[0-9]{1}";
	
	public static boolean checkID(String id){
		Pattern idchecker = Pattern.compile(ID);
		Matcher match = idchecker.matcher(id);
		if(match.matches()) return true;
		else return false;
	}
	
	public static boolean checkYear(String year){
		Pattern yearChecker1 = Pattern.compile(YEAR19xx);
		Pattern yearChecker2 = Pattern.compile(YEAR2000);
		Matcher match1 = yearChecker1.matcher(year);
		Matcher match2 = yearChecker2.matcher(year);
		if(match1.matches()||match2.matches()) return true;
		else return false;
	}
	
	public static boolean checkType(String type){
		Pattern typeChecker = Pattern.compile(TYPE);
		Matcher match = typeChecker.matcher(type);
		if(match.matches()) return true;
		else return false;
	}
}
