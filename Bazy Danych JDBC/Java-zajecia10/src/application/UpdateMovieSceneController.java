package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.*;

import javax.management.relation.RelationServiceNotRegisteredException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class UpdateMovieSceneController {
	@FXML TextField idField;
	@FXML TextField titleField;
	@FXML TextField yearField;
	@FXML TextField typeField;
	@FXML TextField directorField;
	@FXML  Button updateButton;
	private Connection conn = null;
	private final String URL = "jdbc:mysql://localhost:3306/moviesschema?autoReconnect=true&useSSL=false";
	private final String USERNAME = "root";
	private final String PASSWORD = "pass";
	private String command1;
	private String command2;
	private String command3;
	private String command4;
	private String lastCommand;
	private Stage thisStage;
	String title, year, type, director,id;
	private Statement stm1 = null;
	private Statement stm2 = null;
	private Statement stm3 = null;
	private Statement stm4 = null;
	
	
	public UpdateMovieSceneController(Stage thisStage, String command1,String command2,String command3,String command4,String lastCommand) {
		this.command1 = command1;
		this.command2 = command2;
		this.command3 = command3;
		this.command4 = command4;
		this.lastCommand = lastCommand;
		this.thisStage = thisStage;
	}
	
	public void updateButtonOnAction(){
		if((id=idField.getText())!=null&&(title = titleField.getText())!=null&&(year = yearField.getText())!=null&&(type = typeField.getText())!=null&&(director = directorField.getText())!=null&&(Walidacja.checkID(id))&&(Walidacja.checkID(type))&&(Walidacja.checkID(director))&&(Walidacja.checkYear(year))){
		try {
			conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			stm1 = conn.createStatement();
			stm1.executeUpdate(command1+"'"+title+"'"+lastCommand+"'"+id+"'");
			stm2 = conn.createStatement();
			
			stm2.executeUpdate(command2+"'"+year+"'"+lastCommand+"'"+id+"'");
			stm3 = conn.createStatement();
			stm3.executeUpdate(command3+"'"+type+"'"+lastCommand+"'"+id+"'");
			stm4 = conn.createStatement();
			stm4.executeUpdate(command4+"'"+director+"'"+lastCommand+"'"+id+"'");
			conn.close();
			thisStage.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else{
			System.out.println("no nie udalo sie");
		}
	}
}
