package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class UpdateSceneController {
	@FXML TextField idTextField;
	@FXML TextField recordTextField;
	@FXML  Button updateButton;
	private Stage thisStage;
	private Connection conn;
	private Statement stm = null;
	private String command1;
	private String command2;
	private String record = null;
	private String id;
	
	private final String URL = "jdbc:mysql://localhost:3306/moviesschema?autoReconnect=true&useSSL=false";
	private final String USERNAME = "root";
	private final String PASSWORD = "pass";
	
	public UpdateSceneController(Stage thisStage, String command1, String command2) {

		this.thisStage = thisStage;
		this.command1 = command1;
		this.command2 = command2;
	}
	
	@FXML public void updateButtonOnAction(){
		try {
			 conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if((record = recordTextField.getText()) != null && (id = idTextField.getText()) != null&&Walidacja.checkID(id)){
			stm = conn.createStatement();
			stm.executeUpdate(command1+"'"+record+"'"+command2+"'"+id+"'");
			}
			conn.close();
			thisStage.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch(NullPointerException e1){
			e1.printStackTrace();
		}
	}
}
