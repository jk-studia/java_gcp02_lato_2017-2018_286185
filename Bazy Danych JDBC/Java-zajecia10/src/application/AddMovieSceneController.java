package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddMovieSceneController {
	@FXML TextField titleField;
	@FXML TextField yearField;
	@FXML TextField typeField;
	@FXML TextField directorField;
	@FXML  Button addButton;
	private Connection conn = null;
	private final String URL = "jdbc:mysql://localhost:3306/moviesschema?autoReconnect=true&useSSL=false";
	private final String USERNAME = "root";
	private final String PASSWORD = "pass";
	private String command;
	private Stage thisStage;
	String title, year, type, director;
	private Statement stm = null;
	
	public AddMovieSceneController(Stage thisStage, String command) {
		this.command = command;
		this.thisStage = thisStage;
	}
	
	public void addButtonOnAction(){
		if((title = titleField.getText())!=null&&(year = yearField.getText())!=null&&(type = typeField.getText())!=null&&(director = directorField.getText())!=null&&(Walidacja.checkID(type))&&(Walidacja.checkID(director))&&(Walidacja.checkYear(year))){
		try {
			conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			stm = conn.createStatement();
			stm.executeUpdate(command+"('"+title+"',"+"'"+year+"',"+"'"+type+"',"+"'"+director+"')");
			conn.close();
			thisStage.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
}
