package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class DeleteSceneController {
	@FXML TextField textField;
	@FXML  Button deleteButton;
	private Stage thisStage;
	private Connection conn = null;
	private Statement stm = null;
	private String command;
	private String record = null;
	
	private final String URL = "jdbc:mysql://localhost:3306/moviesschema?autoReconnect=true&useSSL=false";
	private final String USERNAME = "root";
	private final String PASSWORD = "pass";
	public DeleteSceneController(Stage thisStage, String command) {

		this.thisStage = thisStage;
		this.command = command;
	}
	
	@FXML public void deleteButtonOnAction(){
		try {
			 conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if((record = textField.getText()) != null){
			stm = conn.createStatement();
			stm.executeUpdate(command+"'"+record+"'");
			}
			conn.close();
			thisStage.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch(NullPointerException e1){
			e1.printStackTrace();
		}
	}
}
