package application;


import java.io.IOException;
import java.sql.*;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class MenuController {
	private Connection conn = null;
	private Stage viewStage  = null;
	private Scene viewScene = null;
	@FXML Button seeButton;
	@FXML Button closeButton;
	@FXML Button editMovieTable;
	@FXML Button editDirectorsTable;
	@FXML Button editTypeTable;
	private FXMLLoader viewLoader = null;
	private ViewSceneController viewController = null;
	private Parent viewLayout = null;
	private Stage primaryStage = null;

	
	public MenuController(Connection conn,  Stage primaryStage) {
		this.conn = conn;
		this.primaryStage = primaryStage;
	}
	
	public void initialize(){
		viewStage = new Stage();
	}
	
	@FXML public void editMovieTableOnAction(){
		try{
		FXMLLoader crudStageLoader = new FXMLLoader();
		Stage crudStage = new Stage();
		crudStageLoader.setLocation(getClass().getResource("CRUDScene.fxml"));
		CRUDController controller = new CRUDController( TableType.MOVIE);
		crudStageLoader.setController(controller);
		Parent layout = crudStageLoader.load();
		Scene crudScene = new Scene(layout);
		crudStage.setScene(crudScene);
		crudStage.setTitle("MovieTable");
		crudStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}		
	}
	
	
	@FXML public void editDirectorsTableOnAction(){
		try{
		FXMLLoader crudStageLoader = new FXMLLoader();
		Stage crudStage = new Stage();
		crudStageLoader.setLocation(getClass().getResource("CRUDScene.fxml"));
		CRUDController controller = new CRUDController( TableType.DIRECTOR);
		crudStageLoader.setController(controller);
		Parent layout = crudStageLoader.load();
		Scene crudScene = new Scene(layout);
		crudStage.setScene(crudScene);
		crudStage.setTitle("DirectorTable");
		crudStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}		
	}
	
	
	@FXML public void editTypeTableOnAction(){
		try{
		FXMLLoader crudStageLoader = new FXMLLoader();
		Stage crudStage = new Stage();
		crudStageLoader.setLocation(getClass().getResource("CRUDScene.fxml"));
		CRUDController controller = new CRUDController( TableType.MOVIETYPE);
		crudStageLoader.setController(controller);
		Parent layout = crudStageLoader.load();
		Scene crudScene = new Scene(layout);
		crudStage.setScene(crudScene);
		crudStage.setTitle("TypeTable");
		crudStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}		
	}
	
	@FXML public void seeButtonOnAction(){

		try {
			ObservableList<MoviesView> viewList = FXCollections.observableArrayList();
			Statement myStatement = conn.createStatement();
			ResultSet myRs;
			myRs = myStatement.executeQuery("select * from moviesview");
			while(myRs.next()){
				MoviesView moviesView = new MoviesView(myRs.getInt(1), myRs.getString(2), myRs.getInt(3), myRs.getString(4), myRs.getString(5));
				viewList.add(moviesView);
			}
			try {
				
				viewStage.setTitle("Database editor");
				viewLoader = new FXMLLoader();
				viewLoader.setLocation(getClass().getResource("ViewScene.fxml"));
				viewController = new ViewSceneController(viewList);
				viewLoader.setController(viewController);
				viewLayout = viewLoader.load();
				viewScene = new Scene(viewLayout);
				viewStage.setScene(viewScene);
				} catch (IOException e) {
					e.printStackTrace();
				}
			viewStage.show();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	@FXML public void closeButtonOnAction(){
		try {
			conn.close();
			if(viewStage.isShowing() && viewStage!=null)
				viewStage.close();
			conn.close();
			primaryStage.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
