package application;
	
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import java.sql.*;
import java.util.List;

public class Main extends Application {
	
	private final String URL = "jdbc:mysql://localhost:3306/moviesschema?autoReconnect=true&useSSL=false";
	private final String USERNAME = "root";
	private final String PASSWORD = "pass";
	@Override
	public void start(Stage primaryStage) {
		try {
		Connection myConn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("Menu.fxml"));
		MenuController controller = new MenuController(myConn, primaryStage);
		loader.setController(controller);
		Parent root = loader.load();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Database editor");
		primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
