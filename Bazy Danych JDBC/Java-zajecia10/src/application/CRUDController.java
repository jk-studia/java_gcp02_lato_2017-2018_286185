package application;

import java.sql.Connection;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class CRUDController {

	@FXML Button addButton;
	@FXML Button showButton;
	@FXML Button updateButton;
	@FXML Button deleteButton;
	MoviesTable moviesTable = null;
	DirectorsTable directorsTable = null;
	MovieTypeTable movieTypeTable = null;
	private int type = -1;
	
	public CRUDController(int type) {
		if(type == TableType.DIRECTOR){
			directorsTable = new DirectorsTable();
		}
		if(type == TableType.MOVIE){
			moviesTable = new MoviesTable();
		}
		if(type == TableType.MOVIETYPE){
			movieTypeTable = new MovieTypeTable();
		}
		this.type = type;
	}
	
	@FXML public void addButtonOnAction(){
		if(type == TableType.DIRECTOR){
			directorsTable.add(type);
		}
		if(type == TableType.MOVIE){
			moviesTable.add();
		}
		if(type == TableType.MOVIETYPE){

			movieTypeTable.add(type);
		}
	}
	@FXML public void showButtonOnAction(){
		if(type == TableType.DIRECTOR){
			directorsTable.show();
		}
		if(type == TableType.MOVIE){
			moviesTable.show();
		}
		if(type == TableType.MOVIETYPE){
			movieTypeTable.show();
		}
	}
	@FXML public void updateButtonOnAction(){
		if(type == TableType.DIRECTOR){
			directorsTable.update();
		}
		if(type == TableType.MOVIE){
			moviesTable.update();
		}
		if(type == TableType.MOVIETYPE){
			movieTypeTable.update();
		}
	}
	@FXML public void deleteButtonOnAction(){
		if(type == TableType.DIRECTOR){
			directorsTable.delete();
		}
		if(type == TableType.MOVIE){
			moviesTable.delete();
		}
		if(type == TableType.MOVIETYPE){
			movieTypeTable.delete();
		}
	}
}
