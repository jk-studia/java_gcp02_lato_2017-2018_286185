package application;

import java.io.IOException;
import java.sql.Connection;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DirectorsTable {
	public DirectorsTable() {
	}
	
	public void add(int type){
		try{
		FXMLLoader addStageLoader = new FXMLLoader();
		Stage addStage = new Stage();
		addStageLoader.setLocation(getClass().getResource("AddScene.fxml"));
		String command = "INSERT INTO `moviesschema`.`directors` (`name`) VALUES ";
		AddSceneController controller = new AddSceneController(addStage, command, type);
		addStageLoader.setController(controller);
		Parent layout = addStageLoader.load();
		Scene addScene = new Scene(layout);
		addStage.setScene(addScene);
		addStage.setTitle("ADD");
		addStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	public void show(){
		
	}
	
	public void update(){
		try{
		FXMLLoader updateStageLoader = new FXMLLoader();
		Stage updateStage = new Stage();
		updateStageLoader.setLocation(getClass().getResource("UpdateScene.fxml"));
		String command1 = "UPDATE `moviesschema`.`directors` SET name = ";
		String command2 = "WHERE iddirectors = ";
		UpdateSceneController controller = new UpdateSceneController(updateStage, command1, command2);
		updateStageLoader.setController(controller);
		Parent layout = updateStageLoader.load();
		Scene deleteScene = new Scene(layout);
		updateStage.setScene(deleteScene);
		updateStage.setTitle("UPDATE");
		updateStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void delete(){
		try{
		FXMLLoader deleteStageLoader = new FXMLLoader();
		Stage deleteStage = new Stage();
		deleteStageLoader.setLocation(getClass().getResource("DeleteScene.fxml"));
		String command = "DELETE FROM `moviesschema`.`directors` WHERE name = ";
		DeleteSceneController controller = new DeleteSceneController(deleteStage, command);
		deleteStageLoader.setController(controller);
		Parent layout = deleteStageLoader.load();
		Scene deleteScene = new Scene(layout);
		deleteStage.setScene(deleteScene);
		deleteStage.setTitle("DELETE");
		deleteStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
