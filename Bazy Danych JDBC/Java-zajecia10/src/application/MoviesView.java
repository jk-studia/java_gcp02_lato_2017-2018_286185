package application;

public class MoviesView {
	private int ID;
	private String title;
	private int year;
	private String type;
	private String name;
	
	public MoviesView(int ID, String title, int year, String type, String name) {
		this.setID(ID);
		this.setTitle(title);
		this.setYear(year);
		this.setType(type);
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
	
	
}
