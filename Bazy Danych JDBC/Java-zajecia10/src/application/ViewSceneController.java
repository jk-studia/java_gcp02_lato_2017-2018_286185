package application;

import java.util.Observable;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ViewSceneController {
	@FXML TableView<MoviesView> tableView;
	@FXML TableColumn<MoviesView, Integer> idColumn;
	@FXML TableColumn<MoviesView, String> titleColumn;
	@FXML TableColumn<MoviesView, Integer> yearColumn;
	@FXML TableColumn<MoviesView, String> typeColumn;
	@FXML TableColumn<MoviesView, String> directorColumn;
	ObservableList<MoviesView> viewList;
	
	public ViewSceneController(ObservableList<MoviesView> viewList) {
		this.viewList = viewList;
	}
	
	@FXML public void initialize(){
		idColumn.setCellValueFactory(new PropertyValueFactory<>("ID"));
		titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
		yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));
		typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
		directorColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		tableView.setItems(viewList);
	}
}
