package application;

import java.io.IOException;
import java.sql.Connection;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MoviesTable {
	public MoviesTable() {
		// TODO Auto-generated constructor stub
	}
	public void add(){
		try{
		FXMLLoader addStageLoader = new FXMLLoader();
		Stage addStage = new Stage();
		addStageLoader.setLocation(getClass().getResource("AddMovieScene.fxml"));
		String command = "INSERT INTO `moviesschema`.`movies` (`title`, `year`, `movietype_idmovietype`, `directors_iddirectors`) VALUES ";
		AddMovieSceneController controller = new AddMovieSceneController(addStage, command);
		addStageLoader.setController(controller);
		Parent layout = addStageLoader.load();
		Scene addScene = new Scene(layout);
		addStage.setScene(addScene);
		addStage.setTitle("ADD");
		addStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	public void show(){
		
	}
	
	public void update(){
		try{
		FXMLLoader updateStageLoader = new FXMLLoader();
		Stage updateStage = new Stage();
		updateStageLoader.setLocation(getClass().getResource("UpdateMovieScene.fxml"));
		String command1 = "UPDATE `moviesschema`.`movies` SET title = ";
		String command2 = "UPDATE `moviesschema`.`movies` SET year = ";
		String command3 = "UPDATE `moviesschema`.`movies` SET movietype_idmovietype = ";
		String command4 = "UPDATE `moviesschema`.`movies` SET directors_iddirectors = ";
		String lastCommand = "WHERE idmovies = ";
		UpdateMovieSceneController controller = new UpdateMovieSceneController(updateStage, command1, command2,command3, command3, lastCommand);
		updateStageLoader.setController(controller);
		Parent layout = updateStageLoader.load();
		Scene deleteScene = new Scene(layout);
		updateStage.setScene(deleteScene);
		updateStage.setTitle("UPDATE");
		updateStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void delete(){
		try{
		FXMLLoader deleteStageLoader = new FXMLLoader();
		Stage deleteStage = new Stage();
		deleteStageLoader.setLocation(getClass().getResource("DeleteScene.fxml"));
		String command = "DELETE FROM `moviesschema`.`movies` WHERE title = ";
		DeleteSceneController controller = new DeleteSceneController(deleteStage, command);
		deleteStageLoader.setController(controller);
		Parent layout = deleteStageLoader.load();
		Scene deleteScene = new Scene(layout);
		deleteStage.setScene(deleteScene);
		deleteStage.setTitle("DELETE");
		deleteStage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
