CREATE VIEW `moviesview` AS
select movies.idmovies, movies.title, movies.year, movietype.type, directors.name
from directors inner join (movietype inner join movies on movietype.idmovietype = movies.movietype_idmovietype) on directors.iddirectors = movies.directors_iddirectors