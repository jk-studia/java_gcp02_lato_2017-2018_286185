package myInterface;

import java.util.Calendar;
import java.util.GregorianCalendar;
import javafx.collections.ObservableList;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;

import myInterface.*;

public class GUILogger  extends UnicastRemoteObject implements Logger {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8690065471275529474L;
	/**
	 * 
	 */
	private LogItem logItem;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
	private Calendar calendar = new GregorianCalendar();
	private ObservableList<LogItem> logList;
	public GUILogger(ObservableList<LogItem> newLogList) throws RemoteException{
		super();
		logList = newLogList;
	}
	@Override
	public void log(String status, Student student){
		logItem = new LogItem();
		if(status == "ADDED"){
			logItem.setMessage(sdf.format(calendar.getTime())+" [NEW] "+student);
			logList.add(logItem);
		}
		if(status == "REMOVED"){
			logItem.setMessage(sdf.format(calendar.getTime())+" [REMOVED] "+student);
			logList.add(logItem);
		}
	}
}
