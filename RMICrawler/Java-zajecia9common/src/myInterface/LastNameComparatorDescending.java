package myInterface;


import java.util.Comparator;

public class LastNameComparatorDescending implements Comparator<Student> {

	@Override
	public int compare(Student student1, Student student2) {
		return student2.getLastName().compareTo(student1.getLastName());
	}
}
