package myInterface;

import java.io.Serializable;

@FunctionalInterface

public interface StudentListener extends Serializable {
	void handle(Student student);
}
