package myInterface;

import java.rmi.Remote;
import java.rmi.RemoteException; 

import java.util.List;

import javafx.event.EventHandler;
import javafx.stage.WindowEvent;


public interface CrawlerMethods extends Remote {
	void addIterationStartedListener(IterationListener listener) throws RemoteException;
	void removeIterationStartedListener(IterationListener listener) throws RemoteException;
	void addIterationCompletedListener(IterationListener listener) throws RemoteException;
	void removeIterationCompletedListener(IterationListener listener) throws RemoteException;
	void addStudentAddedListener(StudentListener listener) throws RemoteException; 
	void removeStudentAddedListener(StudentListener listener) throws RemoteException;
	void addStudentRemovedListener(StudentListener listener) throws RemoteException;
	void removeStudentRemovedListener(StudentListener listener) throws RemoteException;
	//List<Student> extractStudents(OrderMode mode) throws RemoteException;
	//int extractAge(ExtremumMode mode) throws RemoteException;
	//void showSorted(List<Student> students) throws RemoteException;
	//double extractMark(ExtremumMode mode) throws RemoteException;
	void run() throws RemoteException;
	void stop() throws RemoteException;
	void setFile(String name) throws RemoteException;
	public void sendMessages() throws RemoteException;
	public void setMessageEvent(MessageEvent event) throws RemoteException;
}
