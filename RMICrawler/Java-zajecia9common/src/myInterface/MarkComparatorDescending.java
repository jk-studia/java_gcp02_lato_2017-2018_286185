package myInterface;


import java.util.Comparator;

public class MarkComparatorDescending implements Comparator<Student> {

	@Override
	public int compare(Student student1, Student student2) {
		return Double.compare(student2.getMark(), student1.getMark());
	}
}
