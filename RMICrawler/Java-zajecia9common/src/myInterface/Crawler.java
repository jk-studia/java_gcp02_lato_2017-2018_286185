package myInterface;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Crawler {
	private List<IterationListener> iterationStartedListeners = new ArrayList<>();
	public void addIterationStartedListener(IterationListener listener){
		iterationStartedListeners.add(listener);
	}
	public void removeIterationStartedListener(IterationListener listener){
		iterationStartedListeners.remove(listener);
	}
	
	private List<IterationListener> iterationCompletedListeners = new ArrayList<>();
	public void addIterationCompletedListener(IterationListener listener){
		iterationCompletedListeners.add(listener);
	}
	public void removeIterationCompletedListener(IterationListener listener){
		iterationCompletedListeners.remove(listener);
	}
	
	private List<StudentListener> studentAddedListeners = new ArrayList<>();
	public void addStudentAddedListener(StudentListener listener){
		studentAddedListeners.add(listener);
	}
	public void removeStudentAddedListener(StudentListener listener){
		studentAddedListeners.remove(listener);
	}
	
	private List<StudentListener> studentRemovedListeners = new ArrayList<>();
	public void addStudentRemovedListener(StudentListener listener){
		studentRemovedListeners.add(listener);
	}
	public void removeStudentRemovedListener(StudentListener listener){
		studentRemovedListeners.remove(listener);
	}
	
	private List<Student> studentsList = null;
	private Set<Student> studentsSet = new HashSet<Student>();
	
	//final Logger loggers = new ConsoleLogger();
	
	void showSorted(List<Student> students){
		for(Student student: students){
			System.out.println(student.getMark()+" "+student.getFirstName()+" "+student.getLastName()+" "+student.getAge());
		}
	}
	
	double extractMark(ExtremumMode mode)
	{
		double max = 0.0;
		double min = 6.0;
		
		if(mode == ExtremumMode.MAX){
			for(Student student : studentsSet){
				if(student.getMark()>max)
					max = student.getMark();
			}
			return max;
		}
		else 
			if(mode == ExtremumMode.MIN){
				for(Student student : studentsSet){
					if(student.getMark()<min)
						min = student.getMark();
				}
			return min;
			}
		return 0;
	}
	
	int extractAge(ExtremumMode mode){
		int max = 0;
		int min = 100;
		
		if(mode == ExtremumMode.MAX){
			for(Student student : studentsSet){
				if(student.getAge()>max)
					max = student.getAge();
			}
			
			return max;
		}
		if(mode == ExtremumMode.MIN){
			for(Student student : studentsSet){
				if(student.getMark()<min)
					min = student.getAge();
			}		
			return min;
		}
		return 0;
	}
	
	List<Student> extractStudents(OrderMode mode){
		List<Student> tempList= new ArrayList<Student>();
	
		for(Student student : studentsSet)
			tempList.add(student);
		
		System.out.print("ORDERED by ");
		
		if(mode == OrderMode.LAST_NAME){		
			System.out.println("LAST NAME");	
			tempList.sort(new LastNameComparatorAscending());
			//tempList.sort(new LastNameComparatorDescending());
			return tempList;
		}
		if(mode == OrderMode.AGE){	
			System.out.println("AGE");
			tempList.sort(new AgeComparatorAscending());
			//tempList.sort(new AgeComparatorDescending());
			return tempList;
		}
		if(mode == OrderMode.MARK){	
			System.out.println("MARK");
			//tempList.sort(new MarkComparatorAscending());
			tempList.sort(new MarkComparatorDescending());
			return tempList;
		}
		return null;
	}
	
	
	File file = null;
	public void setFile(String name){
		file = new File(name);
	}
	boolean isWorking = true;
	public void stop() throws RemoteException{
		isWorking = false;
	}
	public void run() throws InterruptedException{
		int iteration = 1;
		
		while(isWorking){
			try{
				studentsList  = StudentsParser.parse(file);
			}
			catch(IOException e){
				System.out.println("Error File");
			}
			
			for(IterationListener el :iterationStartedListeners){
				el.handle(iteration);
			}
			
			for(StudentListener el: studentAddedListeners){
				
				for(Student student : studentsList){	
					if(studentsSet==null){
						studentsSet.add(student);
						el.handle(student);
					}
					
				else if(!studentsSet.contains(student)){
						studentsSet.add(student);
						el.handle(student);
					}
						
				}
				
			}
			for(StudentListener el : studentRemovedListeners){
				List<Student> studentsToRemove = new ArrayList<Student>();
				
				for(Student student : studentsSet)
				{
					if(studentsList == null)
					{
						studentsToRemove.add(student);
						for(StudentListener e : studentRemovedListeners){
							e.handle(student);
						}
					}
					else if(!studentsList.contains(student))
					{
						studentsToRemove.add(student);
						for(StudentListener e : studentRemovedListeners){
							e.handle(student);
						}
					}
				}
				
				for(Student student : studentsToRemove)
					studentsSet.remove(student);
			}
			Thread.sleep(10000);
			for(IterationListener el :iterationCompletedListeners){
				el.handle(iteration);
			}
			System.out.println("\n");
			iteration++;
		}
	}
}
