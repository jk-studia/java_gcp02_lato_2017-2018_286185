package myInterface;

import java.io.Serializable;

@FunctionalInterface

public interface IterationListener extends Serializable  {
	void handle(int i);
}
