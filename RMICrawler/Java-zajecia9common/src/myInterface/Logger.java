package myInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.Remote;


public interface Logger  {
	void log(String status, Student student);
}
