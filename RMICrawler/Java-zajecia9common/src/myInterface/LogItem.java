package myInterface;

public class LogItem {
	private String message;

	public LogItem(){
		this.message="";
	}
	public LogItem(String message){
		this.message=message;	
	}
	public void setMessage(String message){
		this.message=message;
	}
	public String getMessage(){
		return message;
	}
}
