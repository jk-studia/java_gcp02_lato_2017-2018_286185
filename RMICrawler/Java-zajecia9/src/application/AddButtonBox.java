package application;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import myInterface.*;

public class AddButtonBox {
	
	public static Student addBox(){
		Student student = new Student();
		Stage addWindow = new Stage();
		addWindow.initModality(Modality.APPLICATION_MODAL);
		addWindow.setTitle("Add Student");
		
		GridPane grid = new GridPane();
		
		Label firstNameLabel = new Label("First Name:");
		GridPane.setConstraints(firstNameLabel, 0, 0);
		TextField firstNameInput = new TextField();
		GridPane.setConstraints(firstNameInput, 1, 0);
		
		Label lastNameLabel = new Label("Last Name:");
		GridPane.setConstraints(lastNameLabel, 0, 1);
		TextField lastNameInput = new TextField();
		GridPane.setConstraints(lastNameInput, 1, 1);
		
		Label markLabel = new Label("Mark:");
		GridPane.setConstraints(markLabel, 0, 2);
		TextField markInput = new TextField();
		GridPane.setConstraints(markInput, 1, 2);
		
		Label ageLabel = new Label("Age:");
		GridPane.setConstraints(ageLabel, 0, 3);
		TextField ageInput = new TextField();
		GridPane.setConstraints(ageInput, 1, 3);
		Button add = new Button("Add");
		add.setOnAction(e->{
			try{
			student.setFirstName(firstNameInput.getText());
			student.setLastName(lastNameInput.getText());
			student.setMark(Double.parseDouble(markInput.getText()));
			student.setAge(Integer.parseInt(ageInput.getText()));
			addWindow.close();
			}catch(Exception e1){
				showError(addWindow);
			}
		});
		GridPane.setConstraints(add, 1, 4);
		
		
		grid.getChildren().addAll(firstNameLabel,firstNameInput,lastNameLabel,lastNameInput,markLabel,markInput,ageLabel,ageInput,add);
		Scene scene = new Scene(grid, 250, 120);
		addWindow.setScene(scene);
		addWindow.setResizable(false);
		addWindow.showAndWait();
		return student;
	}
	
	public static void showError(Stage addWindow){
        Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot Add Student" , ButtonType.OK);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.setTitle("Error");
        alert.setHeaderText("Details");
        alert.setContentText("Cannot Add Student");
        alert.initOwner(addWindow);
        alert.show();
	}
}
