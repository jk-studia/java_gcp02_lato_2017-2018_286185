package application;
	
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;

import myInterface.*;
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception{
			primaryStage.setTitle("CrawlerGUI");

			ObservableList<LogItem> logList = FXCollections.observableArrayList();
			ObservableList<Student> observableStudentList = FXCollections.observableArrayList();
			Logger logger =  new GUILogger(logList);			

			// creating scene
			VBox layout = new VBox();
			CustomMenuBar customMenuBar = new CustomMenuBar(primaryStage);
			CustomTabPane customTabPane = new CustomTabPane(primaryStage, logList, logger, observableStudentList);
			layout.getChildren().addAll(customMenuBar, customTabPane);
			Scene scene = new Scene(layout, 500, 500);
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
