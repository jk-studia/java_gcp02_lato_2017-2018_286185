package application;

import java.io.Serializable;
import java.rmi.AccessException;
import myInterface.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javafx.collections.ObservableList;

import myInterface.CrawlerMethods;
public class MyThread extends Thread{

	private Logger logger;
	private CustomBarChart customBarChart;
	private ObservableList<Student> studentList = null;
	public CrawlerMethods crawler = null;
	public MyThread(Logger newLogger, CustomBarChart newCustomBarChart, ObservableList<Student> newStudentList){
		logger = newLogger;
		customBarChart = newCustomBarChart;
		studentList = newStudentList;
	}
	
	@Override
	public void run(){
		String host = "localhost";
		int port = 6000;
		String name = "Crawler" + port;
		Registry registry = null;
		try {
			registry = LocateRegistry.getRegistry( host, port );
		} catch (RemoteException e1) {
			e1.printStackTrace();
		}
		
		try {
			crawler = (CrawlerMethods) registry.lookup(name);;
		} catch (AccessException e2) {
			System.out.println("Error: mythread->access");
		} catch (RemoteException e2) {
			System.out.println("Error: mythread->remote");
		} catch (NotBoundException e2) {
			System.out.println("Error: mythread->notbound");
		}
		try {
			crawler.setFile("students1.txt");
			crawler.setMessageEvent(new CustomMessageEvent());
			crawler.addIterationStartedListener((IterationListener & Serializable)(iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
			crawler.addIterationCompletedListener((IterationListener & Serializable)(iteration)->System.out.println("Zakonczono iteracje"));
			crawler.addStudentAddedListener((StudentListener & Serializable)(Student)->{logger.log("ADDED", Student);studentList.add(Student); customBarChart.update(studentList);});
			crawler.addStudentRemovedListener((StudentListener & Serializable)(Student)->{logger.log("REMOVED", Student);studentList.remove(Student); customBarChart.update(studentList);});
			crawler.run();
		} catch (RemoteException e1) {
			
			e1.printStackTrace();
		}
	}
}
