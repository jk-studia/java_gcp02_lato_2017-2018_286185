package application;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import myInterface.MessageEvent;

public class CustomMessageEvent extends UnicastRemoteObject implements MessageEvent
{
	private static final long serialVersionUID = 6738492455439057283L;
	
	protected CustomMessageEvent() throws RemoteException
	{
		super();
	}
	
	@Override
	public void messageSended( String message )
	{
		System.out.printf( "Server message: %s\n", message );
	}
}
