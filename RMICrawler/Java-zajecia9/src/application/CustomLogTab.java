package application;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import myInterface.LogItem;

public class CustomLogTab extends AnchorPane{
	private TableView<LogItem> tableView;
	private TableColumn<LogItem, String> columnLog;
	
	public CustomLogTab(Stage primaryStage, ObservableList<LogItem> logList){
		tableView = new TableView<LogItem>();
		columnLog = new TableColumn<>();
		columnLog.setMinWidth(400);
		columnLog.setCellValueFactory(new PropertyValueFactory<>("message"));
		tableView.getColumns().add(columnLog);
		tableView.setItems(logList);
		
		tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		this.getChildren().addAll(tableView);
		CustomLogTab.setTopAnchor(tableView, 10.0);
		CustomLogTab.setLeftAnchor(tableView, 10.0);
		CustomLogTab.setRightAnchor(tableView, 10.0);
		CustomLogTab.setBottomAnchor(tableView, 10.0);
	}
}
