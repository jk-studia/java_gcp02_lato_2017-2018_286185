package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import myInterface.Crawler;
import myInterface.CrawlerMethods;
import myInterface.IterationListener;
import myInterface.MessageEvent;
import myInterface.StudentListener;

public class RMICrawlerProxy extends UnicastRemoteObject implements CrawlerMethods {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6967044876636632201L;
	private Crawler crawler = new Crawler();
	private MessageEvent event = null;
	protected RMICrawlerProxy() throws RemoteException {
		super();
	}
	
	@Override
	public void addIterationStartedListener(IterationListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.addIterationStartedListener(listener);;
		
	}

	@Override
	public void removeIterationStartedListener(IterationListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.removeIterationStartedListener(listener);
		
	}

	@Override
	public void addIterationCompletedListener(IterationListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.addIterationCompletedListener(listener);
		
	}

	@Override
	public void removeIterationCompletedListener(IterationListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.removeIterationCompletedListener(listener);
	}

	@Override
	public void addStudentAddedListener(StudentListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.addStudentAddedListener(listener);
		
	}

	@Override
	public void removeStudentAddedListener(StudentListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.removeStudentAddedListener(listener);
	}

	@Override
	public void addStudentRemovedListener(StudentListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.addStudentRemovedListener(listener);
	}

	@Override
	public void removeStudentRemovedListener(StudentListener listener) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.removeStudentRemovedListener(listener);
	}

	@Override
	public void run() throws RemoteException {
		// TODO Auto-generated method stub
		try {
			crawler.run();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void stop() throws RemoteException {
		// TODO Auto-generated method stub
		crawler.stop();
	}

	@Override
	public void setFile(String name) throws RemoteException {
		// TODO Auto-generated method stub
		crawler.setFile(name);
	}

	@Override
	public void sendMessages() throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMessageEvent(MessageEvent event) throws RemoteException {
		// TODO Auto-generated method stub
		this.event = event;
	}

}
