package server;

import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int port = 6000;
		String name = "Crawler" + port;
		Registry registry = null;
		try {
			registry = LocateRegistry.createRegistry( port );
		} catch (RemoteException e) {
			System.out.println("Error: main->registry");
		}
		RMICrawlerProxy crawlerProxy = null;
		try {
			crawlerProxy = new RMICrawlerProxy();
		} catch (RemoteException e) {
			System.out.println("Error: main->crawlerProxy");
		}

		try {
			registry.bind( name, crawlerProxy );
		} catch (AccessException e) {
			System.out.println("Error: main->access");
		} catch (RemoteException e) {
			System.out.println("Error: main->registry bind remote");
		} catch (AlreadyBoundException e) {
			System.out.println("Error: main->already bound");
		}

		try{
			Scanner scan = null;
			System.out.println("Server start");
			String shutdown = null;
			scan = new Scanner(System.in);
			while(true){
				shutdown = scan.nextLine();
				if(shutdown.equals("shutdown"))
					try {
						crawlerProxy.stop();
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
			}
			scan.close();
		}
		finally{
			try {
				UnicastRemoteObject.unexportObject( registry, true );
			} catch (NoSuchObjectException e) {
				e.printStackTrace();
			}
			System.out.println( "Server stopped." );
			System.exit(0);
		}
	}

}
